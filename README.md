# Open Perudo

Ce projet a pour objectif de créer une version en ligne du jeu `Perudo` dont les règles sont disponibles ici : https://www.jeuxavolonte.asso.fr/regles/perudo.pdf

## Architecture

Cette application se divise en deux modules applicatifs :

- OpenPerudo-UI : frontend React / React native
- OpenPerudo-API : backend en python (fastapi)

## Contributions

L'objectif principal de ce projet est de fournir, aux étudiants qui le souhaitent, un cas réel, concret et intéressant pour mettre en application les concepts vus lors de deux cours, l'un de développement mobile à partir de technologies Web (React / React native), l'autre un cours de conception logicielle illustrant les bonnes pratiques d'ingénierie de développement.
La façon de contribuer est décrite dans le document [CONTRIBUTING.md](CONTRIBUTING.md)

## Getting started (comment utiliser OpenPerudo-API)

TODO : comment utiliser / déployer OpenPerudo-API ?
